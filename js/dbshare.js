jQuery('#edit-location-value').before('<small id="edit-location-suggestion" class"form-text"></small>');
function GetMap() {
    Microsoft.Maps.loadModule('Microsoft.Maps.AutoSuggest', {
        callback: function () {
            var manager = new Microsoft.Maps.AutosuggestManager({
                placeSuggestions: true,
                maxResults: 4,
                addressSuggestions: false
            });
            manager.attachAutosuggest('#edit-location-value', '#edit-location-wrapper', selectedSuggestion);
            // delete style attribute just added by bing
            jQuery('#edit-location-value').removeAttr("style");
        },
        errorCallback: function(msg){
          document.getElementById('edit-location-wrapper').innerHTML = msg;
        }
    });
}

function selectedSuggestion(suggestionResult) {
    document.getElementById('edit-location-suggestion').innerHTML = suggestionResult.location.latitude + ',' + suggestionResult.location.longitude;
}

// We have to rezoom the search map because it is hidden by bootstrap tab initialy.
// https://stackoverflow.com/questions/39633717/two-leaflet-maps-in-two-different-bootstrap-tabs
// https://stackoverflow.com/questions/20705905/bootstrap-3-jquery-event-for-active-tab-change/29184836#29184836
// https://stackoverflow.com/questions/36246815/data-toggle-tab-does-not-download-leaflet-map/36257493#36257493
var isZoomed = false;
jQuery('a#map-tab[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  if (! isZoomed) {
    // fire a resize, so map can redefine its display (if not the map is a small square when in a tab)
    window.dispatchEvent(new Event('resize'));
    jQuery.each(drupalSettings.leaflet, function(m, data) {
      jQuery('#' + data.mapid).each(function() {
        let container = jQuery(this);
        let mapid = data.mapid;
        Drupal.Leaflet[mapid].lMap.setZoom(4);
        isZoomed = true;
      });
    });
  }
});
