/**
 * @file
 * Attaches several event listener to a web page.
 */

(function ($, drupalSettings) {

  "use strict";

  $(document).ready(function () {

    // Attach mousedown, keyup, touchstart events to document only and catch
    // clicks on all elements.
    $(document.body).bind("mousedown keyup touchstart", function (event) {

      // Catch the closest surrounding link of a clicked element.
      $(event.target).closest("a,area").each(function () {

        if ($(this).is("a[href^='tel:'],area[href^='tel:']")) {
          // Tel link clicked.
          _paq.push(["trackEvent", "Phones", "Click", decodeURI(this.href.substring(4))]);
        }

      });

      // Catch the juicebox gallery if it exists
      $(event.target).closest(".juicebox-gallery").each(function () {
          _paq.push(["trackEvent", "Images", "Click", $('.jb-dt-main-image-0 img').first().attr('src')]);
      });

    });

    var tracked_uris = [];

    // Track all videos in the page. Clicked or not (in iframe we cannot know if it is clicked or not)
    $("iframe.media-oembed-content").each(function () {
      var video_uri = decodeURI($(this).attr('src').replace(/.*media\/oembed\?url=(.*)&max_width.*/, '$1'));
      if (video_uri.length > 0 && tracked_uris.indexOf(video_uri) === -1) {
        tracked_uris.push(video_uri);
        _paq.push(["trackEvent", "Videos", "Load", video_uri]);
      }
    });

    $(document).bind("cbox_complete", function () {
      var href = $.colorbox.element().attr("href");
      if (href) {
        _paq.push(["trackEvent", "Images", "Click", href]);
      }
    });
  });

})(jQuery, drupalSettings);
